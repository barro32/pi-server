require('dotenv').config()
const express = require('express')
const app = express()

require('./modules/ip')
const camera = require('./modules/camera')
const controlPanel = require('./modules/control-panel')

app.get('/p', camera.toggleRecord)
app.get('/photo', camera.display)
app.get('/cp', controlPanel)

app.listen(3000)

//ffmpeg -framerate 24 -pattern_type glob -i '*.jpg' -pix_fmt yuv420p vid.mp4
