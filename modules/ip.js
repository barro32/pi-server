const fetch = require('node-fetch')
const nodemailer = require('nodemailer')

const ipInterval = 1000 * 60 * 60 * 24
let ip

const sendEmail = (ip) => {
    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        auth: {
            user: process.env.SMTP_USER,
            pass: process.env.SMTP_PASS
        }
    });
    let message = {
        from: 'pi@node.com',
        to: 'daniel.barrington@gmail.com',
        subject: 'New IP',
        text: ip
    }

    transporter.sendMail(message)
}

const getIp = async () => {
    const res = await fetch('http://checkip.amazonaws.com')
    const newIp = await res.text()
    if (newIp !== ip) {
        sendEmail(newIp)
        ip = newIp
        console.log(ip)
    }
}

getIp()
setInterval(getIp, ipInterval)

module.exports = getIp
