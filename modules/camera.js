const moment = require('moment')
const fs = require('fs')
const { exec } = require('child_process')

const interval = 7000
const width = 1024
const height = 768
const quality = 6

let record = true
let shutterSpeed = 500000
let iso = 800
const baseDir = '/home/pi/node/images/'
let dir
let image
let prevImage

let exTimeout = 0

const camera = {
    toggleRecord: (req, res) => {
        record = !record
        res.send(record)
    },
    display: (req, res) => {
        const data = fs.readFileSync(`${dir}/${prevImage}.jpg`)
        const img64 = Buffer.from(data).toString('base64')
        res.send(`<img src="data:image/jpeg;base64,${img64}">`)
    },
    takePicture: () => {
        if (record) {
            camera.checkDir()
            image = moment().format('HH-mm-ss')
            console.log('iso', iso, 'ss', shutterSpeed)
            exec(`raspistill -n -o ${dir}/${image}.jpg -q ${quality} -w ${width} -h ${height} -ISO ${iso} -ss ${shutterSpeed} && echo ${image}`, (error, stdout, stderr) => {
                console.log('done?')
                if (!error) {
                    prevImage = stdout.replace(/\s/g, "")
                    camera.checkExposure(prevImage)
                } else {
                    console.log('Image Error:', error)
                }
            })
        }
        setTimeout(camera.takePicture, (shutterSpeed / 1000) + interval)
    },
    checkDir: () => {
        const newDir = baseDir + moment().format('YYYY-MM-DD')

        const createNewDir = dirName => {
            fs.mkdirSync(dirName)
            dir = dirName
        }

        if (!dir || dir !== newDir) {
            if (!fs.existsSync(newDir)) {
                createNewDir(newDir)
            } else {
                dir = newDir
            }
        }
    },
    checkExposure: (img) => {
        console.log('t', exTimeout, 'img', img)
        if (exTimeout-- <= 0) {
            exec(`convert ${dir}/${img}.jpg -colorspace Gray -format "%[fx:image.mean]" info:`, (error, stdout, stderr) => {
                if (!error) {
                    camera.adjustExposure(stdout)
                } else {
                    console.log('Exposure Error:', error)
                }
            })
        }
    },
    adjustExposure: (value = 0.9) => {
        console.log('val', value)
        const brightness = Number(value)
        // if (brightness <= 0.2) {
        //     if (iso < 800) iso *= 2
        // } else if (brightness >= 0.8) {
        //     if (iso > 100) iso /= 2
        // }
        // if (brightness <= 0.4) {
        //     shutterSpeed = Math.floor(shutterSpeed * 1.1)
        //     if (shutterSpeed > 1000000 && iso < 800) {
        //         shutterSpeed /= 2
        //         iso *= 2
        //     }
        // } else if (brightness >= 0.5) {
        //     shutterSpeed = Math.floor(shutterSpeed * 0.9)
        //     if (shutterSpeed < 50000 && iso > 100) {
        //         shutterSpeed *= 2
        //         iso /= 2
        //     }
        // } else {
        //     exTimeout = 10
        // }
        const factor = (brightness - 1.5) * -1
        console.log('factor', factor)
        shutterSpeed = Math.floor(shutterSpeed * factor)
    }
}
camera.takePicture()
// setInterval(camera.takePicture, interval)

module.exports = camera
