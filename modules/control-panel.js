const cp = (req, res) => {
    const template = `
        <div class="js-recording"></div>
        <button class="js-record">Start / Stop</button>
        <script>
            const btn = document.querySelector('.js-record')
            const container = document.querySelector('.js-recording')
            btn.addEventListener('click', async () => {
                const res = await fetch(window.location.origin + '/p')
                container.textContent = await res.text()
            })
        </script>
    `
    res.send(template)
}

module.exports = cp
